import request from "@/utils/request.js";
import {appid} from '@/utils/conf.js'


export function getInfo(data) {
	return request({
		url: `/jeecg-boot/weChat/getInfo`,
		method: 'post',
		data
	})
}

export function loginByCode(code) {
	return request({
		url: '/jeecg-boot/weChat/point/code',
		method: 'post',
		data: {
			code,
			appid
		}
	})
}

export function getAuth(data) {
	return request({
		url: `/jeecg-boot/weChat/point/info`,
		method: 'post',
		data: {
			...data,
			appid
		}
	})
}

export function login(data) {
	return request({
		url: `/jeecg-boot/weChat/point/login`,
		method: 'post',
		data: {
			...data,
			appid
		}
	})
}

export function propertyPointBanner() {
	return request({
		url: '/jeecg-boot/points/propertyPointBanner/list/mp',
		method: 'get',
	})
}

export function propertyPointNotice() {
	return request({
		url: '/jeecg-boot/points/propertyPointNotice/list/mp',
		method: 'get',
	})
}

export function propertyPointGoodsInfo(data) {
	return request({
		url: '/jeecg-boot/points/propertyPointGoodsInfo/list/mp',
		method: 'get',
		data
	})
}

export function queryById(data) {
	return request({
		url: '/jeecg-boot/points/propertyPointGoodsInfo/queryById',
		method: 'get',
		data
	})
}

export function perfect(data) {
	return request({
		url: '/jeecg-boot/points/propertyPointAccount/perfect',
		method: 'post',
		data
	})
}

export function userQueryById(data) {
	return request({
		url: '/jeecg-boot/points/propertyPointAccount/queryById',
		method: 'get',
		data
	})
}

export function exchange(data) {
	return request({
		url: '/jeecg-boot/points/propertyPointGoodsRecord/add',
		method: 'post',
		data
	})
}

export function propertyPointGoodsRecord(data) {
	return request({
		url: '/jeecg-boot/points/propertyPointGoodsRecord/list/mp',
		method: 'get',
		data
	})
}

export function propertyPointQueryById(data) {
	return request({
		url: '/jeecg-boot/points/propertyPointGoodsRecord/queryById',
		method: 'get',
		data
	})
}


export function propertyPointChangeRecord(data) {
	return request({
		url: '/jeecg-boot/points/propertyPointChangeRecord/list/mp',
		method: 'get',
		data
	})
}

export function rank(data) {
	return request({
		url: '/jeecg-boot/points/propertyPointAccount/rank',
		method: 'get',
		data
	})
}

export function propertyPointAddress(data) {
	return request({
		url: '/jeecg-boot/points/propertyPointAddress/list/mp',
		method: 'get',
		data
	})
}

export function propertyPointQuestion(data) {
	return request({
		url: '/jeecg-boot/points/propertyPointQuestion/list/mp',
		method: 'get',
		data
	})
}

export function logout(data) {
	return request({
		url: '/jeecg-boot/weChat/point/logout',
		method: 'post',
		data
	})
}


export function getPointInfo(data) {
	return request({
		url: '/jeecg-boot/points/propertyPointGoodsRecord/getInfo',
		method: 'post',
		data
	})
}

export function addPointInfo(data) {
	return request({
		url: '/jeecg-boot/points/propertyPointGoodsRecord/exchange',
		method: 'put',
		data
	})
}

export function exchangeList(data) {
	return request({
		url: '/jeecg-boot/points/propertyPointGoodsRecord/exchangeList',
		method: 'get',
		data
	})
}


